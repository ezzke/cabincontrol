#include "stdafx.h"
#include "State.h"

using namespace Pattern;

void StateContext::ChangeState(IState* newState)
{
	if ( newState != 0 )
	{
		state = newState;
		state->StateChanged();
	}
}

void State::ChangeState(IStateContext* stateContext, IState* newState)
{
	stateContext->ChangeState(newState);
}

IState* State::GetNextState(IStateParameters* stateParameters)
{
	list<IState*>::iterator linkedStatesIterator = linkedStates.begin();
	while (linkedStatesIterator != linkedStates.end())
	{
		IState* linkedState = *linkedStatesIterator;
		if ( linkedState->GetStateParameters()->Equals(stateParameters) )
		{
			return linkedState;
		}
		linkedStatesIterator++;
	}
	return 0;
}

void State::SetStateParameters(IStateParameters* stateParameters)
{
	this->stateParameters = stateParameters;
}

IStateParameters* State::GetStateParameters()
{
	return stateParameters;
}

void State::SetLinkedStates(list<IState*> linkedStates)
{
	this->linkedStates = linkedStates;
}

void State::SetStateChangedEvents(vector<ParameterizedCommandPair*> stateChagnedEvents)
{
	this->stateChagnedEvents = stateChagnedEvents;
}

void State::StateChanged()
{
	IParameterizedCommand* command;
	IActionParameters* eventParameters = 0;
	for (size_t i = 0; i < this->stateChagnedEvents.size(); i++)
	{
		command = stateChagnedEvents[i]->GetCommand();
		eventParameters = stateChagnedEvents[i]->GetActionParameters();
		if ( !command->IsEmpty() )
		{
			(*command)(eventParameters);
		}
	}
}