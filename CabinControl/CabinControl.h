#ifdef CABINCONTROL_EXPORTS
	#define CABINCONTROL_API __declspec(dllexport)
#else
	#define CABINCONTROL_API __declspec(dllimport)
#endif

#include "CabinController.h"

using namespace Cabin;

// ������ ���������� � ���������� ��������� �� ����
CABINCONTROL_API void* CABIN_CreateCabinController(int cabinCount);

// ���������� ������ � ����������
CABINCONTROL_API int CABIN_Send(void *cabinController, void *data);

// ������������� ���������� ��� ��������� ������ �� �����������
CABINCONTROL_API int CABIN_SetRequestHandler(void *cabinController, RequestHandler handler);