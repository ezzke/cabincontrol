#pragma once
#include "Command.h"

using Pattern::IActionParameters;

namespace Cabin
{

	enum MessageCode
	{
		START,
		STOP,
		PAUSE,
		SET_CABIN_OPTION
	};

	class MessageDataBase : public IActionParameters
	{
	public:
		int code;
	};

	class MessageHandlerStateMsg : public MessageDataBase
	{
	};

	class CabinOptionMsg : public MessageDataBase
	{
	public:
		int option;
		int value;
	};

	class MessageParser
	{
	public:
		// ����������� void* � ���������� �������
		static MessageDataBase* Parse(void*);
	};

}