#include "stdafx.h"
#include "MessageData.h"

using namespace Cabin;

MessageDataBase* MessageParser::Parse(void *data)
{
	MessageDataBase* message = 0;
	if (data != 0)
	{
		int type = *reinterpret_cast<int*>(data);
		switch(type)
		{
		case MessageCode::START:
			message = new MessageHandlerStateMsg;
			memcpy(message, data, sizeof(MessageHandlerStateMsg));
			break;
		case MessageCode::STOP:
			message = new MessageHandlerStateMsg;
			memcpy(message, data, sizeof(MessageHandlerStateMsg));
			break;
		case MessageCode::PAUSE:
			message = new MessageHandlerStateMsg;
			memcpy(message, data, sizeof(MessageHandlerStateMsg));
			break;
		case MessageCode::SET_CABIN_OPTION:
			message = new CabinOptionMsg;
			memcpy(message, data, sizeof(CabinOptionMsg));
			break;
		}
	}
	return message;
}
