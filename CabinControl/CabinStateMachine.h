#pragma once
#include "State.h"
#include "Command.h"
//#include "Event.h"
#include <vector>
#include <map>

using namespace Pattern;

namespace Cabin
{

	using namespace std;

	enum CabinNumber
	{
		First,
		Second
	};

	enum CabinType
	{
		SINGLE = 1,
		DOUBLE = 2
	};

	enum CabinOptions
	{
		DOOR1 = 0,
		SENSOR1 = 1,
		SENSOR2 = 2,
		DETECTOR1 = 3,
		SENSOR3 = 4,
		DOOR2 = 5,
		SENSOR4 = 6,
		SENSOR5 = 7,
		DETECTOR2 = 8,
		SENSOR6 = 9,
		DOOR3 = 10,
		SENSOR7 = 11
	};

	class BitUtils
	{
	public:
		static vector<int> unpack(unsigned int val, size_t count);
	};

	class CabinDevicesStateActionParams : public IActionParameters
	{
	public:
		int optionIndex;
		int optionValue;

		CabinDevicesStateActionParams(int optionIndex, int optionValue);
	};

	class CabinSensorStateActionParams : public IActionParameters
	{
	public:
		int stateMachineId;
		int stateNumber;

		CabinSensorStateActionParams(int stateMachineId, int stateNumber);
	};

	class CabinStateParameters : public StateParameters
	{
	private:
		vector<int> cabinOptions;

	public:
		bool Equals(IStateParameters* stateParameters);
		void SetCabinOptions(vector<int> cabinOptions);
		void SetCabinOptions(unsigned int packedCabinOptions, size_t optionsCount);
		vector<int> GetCabinOptions();
		void SetCabinOption(int optionIndex, int optionValue);
	};

	class CabinState : public State
	{
	public:
		virtual void Execute(IStateContext* stateContext, IStateParameters* stateParameters);
	};

	class CabinStateMachine : public StateContext
	{
	public:
		CabinStateMachine();
		~CabinStateMachine();

		void Execute(IStateParameters* stateParameters);
	};

	class CabinSensorsState : public CabinState
	{
	};

	class CabinSensorsStateParameters : public CabinStateParameters
	{
	};

	class CabinSensorsStateMachine : public CabinStateMachine   
	{
	};

	class UpdateCabinDevicesStateActionParameters : public IActionParameters
	{
	public:
		IStateContext* stateContext;
		IStateParameters* stateParameters;

		UpdateCabinDevicesStateActionParameters(IStateContext* stateContext, IStateParameters* stateParameters);
	};

	class UpdateOptionActionParameters : public IActionParameters
	{
	public:
		int optionIndex;
		int optionValue;

		UpdateOptionActionParameters(int optionIndex, int optionValue);
	};

	class DevicesCommandPair
	{
	public:
		IStateParameters* stateParameters;
		ParameterizedCommandPair* parameterizedCommandPair;

		DevicesCommandPair(IStateParameters* stateParameters, ParameterizedCommandPair* parameterizedCommandPair);
	};

	class CabinDevicesState : public CabinState
	{
	private:
		vector<DevicesCommandPair*> commands;
	public:
		void SetCommands(vector<DevicesCommandPair*> commands);
		void Execute(IStateContext* stateContext, IStateParameters* stateParameters);
	};

	class CabinDevicesStateParameters : public CabinStateParameters
	{
	};

	class CabinDevicesStateMachine : public CabinStateMachine
	{
	};

	class CabinBase
	{
	protected:
		vector<CabinSensorsStateMachine*> cabinSensorsStateMachines;
		CabinDevicesStateMachine* devicesStateMachine;

		CabinDevicesStateParameters* cabinDevicesStateParameters;
		vector<int> cabinSensorsStateNumbers;

	public:
		virtual void Execute(IStateParameters* stateParameters) = 0;
		void UpdateCabinSensorsStateNumbers(IActionParameters* cabinSensorsEventParameters);

		void ResetCabinSensorsStateMachine(int sensorsStateMachineNumber, IState* state);
		void ResetDevicesStateMachine(IState* state);

		void SetCabinSensorsStateMachines(vector<CabinSensorsStateMachine*> cabinSensorsStateMachines);
		void SetDeviceStateMachine(CabinDevicesStateMachine* devicesStateMachine);
		void SetCabinSensorsStateNumbers(vector<int> cabinSensorsStateNumbers);
		void SetCabinDevicesStateParameters(CabinDevicesStateParameters* cabinDevicesStateParameters);

		CabinBase();
		~CabinBase();
	};

	class SingleCabin : public CabinBase
	{
	public:
		void Execute(IStateParameters* stateParameters);

		SingleCabin();
		~SingleCabin();
	};
	
	class DoubleCabin : public CabinBase
	{
	public:
		void Execute(IStateParameters* stateParameters);

		DoubleCabin();
		~DoubleCabin();
	};

}