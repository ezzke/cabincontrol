// StateMachineTest.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "StateMachineTest.h"

typedef void (*Handler)(void *data);

typedef void* (*fpCreateCabinController)(int);
typedef int (*fpSetRequestHandler)(void*, Handler);
typedef int (*fpSend)(void*, void*);

#define LIBNAME L"CabinControl.dll"
#define CREATE_CABIN_CONTROLLER "CABIN_CreateCabinController"
#define SEND "CABIN_Send"
#define SET_HANDLER "CABIN_SetRequestHandler"

HINSTANCE hInstLib = 0;
fpCreateCabinController fCreateCabinController = 0;
fpSend fSend = 0;
fpSetRequestHandler fSetRequest = 0;
void* controller;

HWND hDlg;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

enum MessageCode
{
	START,
	STOP,
	PAUSE,
	SET_CABIN_OPTION
};

class IActionParameters
{
};

class MessageDataBase : public IActionParameters
{
public:
	int code;
};

class MessageHandlerStateMsg : public MessageDataBase
{
};

class CabinOptionMsg : public MessageDataBase
{
public:
	int option;
	int value;
};

class MessageParser
{
public:
	// ����������� void* � ���������� �������
	static MessageDataBase* Parse(void*);
};


void ResponseHandler(void *data)
{
	CabinOptionMsg* message = 0;
	if (data != 0)
	{
		int type = *reinterpret_cast<int*>(data);
		switch(type)
		{
		case MessageCode::SET_CABIN_OPTION:
			{
				message = new CabinOptionMsg;
				memcpy(message, data, sizeof(CabinOptionMsg));


				wchar_t* command_string = L"";
				switch(message->option)
				{
				case 0:
					if (!message->value)
						command_string = L"D1_CLOSE";
					else
						command_string = L"D1_OPEN";
					break;
				case 3:
					command_string = L"W1_ENABLE";
					break;
				case 5:
					if (!message->value)
						command_string = L"D2_CLOSE";
					else
						command_string = L"D2_OPEN";
					break;
				case 8:
					command_string = L"W2_ENABLE";
					break;
				case 10:
					if (!message->value)
						command_string = L"D3_CLOSE";
					else
						command_string = L"D3_OPEN";
					break;
				}
				SetDlgItemText(hDlg, IDC_STATIC666, command_string);
			}
			break;
		}
	}
}

int LoadCabinControlLibrary(int cabinCount)
{

	hInstLib = LoadLibrary( LIBNAME );
	if ( !hInstLib )
	{
		return 1;
	}

	fCreateCabinController = ( fpCreateCabinController ) GetProcAddress( hInstLib, CREATE_CABIN_CONTROLLER );
	if ( !fCreateCabinController )
	{
		return 1;
	}

	fSend= ( fpSend ) GetProcAddress( hInstLib, SEND );
	if ( !fSend )
	{
		return 1;
	}

	fSetRequest = ( fpSetRequestHandler ) GetProcAddress( hInstLib, SET_HANDLER );
	if ( !fSetRequest )
	{
		return 1;
	}

	controller = fCreateCabinController(cabinCount);
	fSetRequest(controller, ResponseHandler);
	return 0;
}

int APIENTRY _tWinMain(_In_ HINSTANCE instance, _In_opt_ HINSTANCE prevInstance, _In_ LPTSTR cmdLine, _In_ int cmdShow)
{

	int loadResult = LoadCabinControlLibrary(2);
	if ( loadResult )
	{
		return loadResult;
	}

	DialogBox(instance, MAKEINTRESOURCE(IDD_TEST_WINDOW),NULL,(DLGPROC)WndProc);

	return 0;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch (message)
	{
		case WM_INITDIALOG:
			hDlg = hWnd;
			SetDlgItemInt(hDlg,IDC_STATIC1,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC2,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC3,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC4,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC5,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC6,1,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC7,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC8,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC9,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC10,0,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC11,1,FALSE);
			SetDlgItemInt(hDlg,IDC_STATIC12,0,FALSE);
			break;
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);

			switch (wmId)
			{
			case IDCANCEL:			
				DestroyWindow(hWnd);
				break;

			case IDC_STOP_BTN:
				{
					MessageHandlerStateMsg* mshm = new MessageHandlerStateMsg;
					mshm->code = MessageCode::STOP;
					fSend(controller, mshm);
					SetDlgItemText(hDlg, IDC_STATIC99, L"STOPPED");
				}
				break;
			case IDC_START_BTN:
				{
					MessageHandlerStateMsg* mshm = new MessageHandlerStateMsg;
					mshm->code = MessageCode::START;
					fSend(controller, mshm);
					SetDlgItemText(hDlg, IDC_STATIC99, L"STARTED");
				}
				break;
			case IDC_PAUSE_BTN:
				{
					MessageHandlerStateMsg* mshm = new MessageHandlerStateMsg;
					mshm->code = MessageCode::PAUSE;
					fSend(controller, mshm);
					SetDlgItemText(hDlg, IDC_STATIC99, L"PAUSED");
				}
				break;

			case IDC_BUTTON2:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC1, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC1, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 0;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON3:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC2, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC2, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 1;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON4:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC3, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC3, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 2;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON5:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC4, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC4, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 3;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON6:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC5, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC5, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 4;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON7:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC6, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC6, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 5;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON8:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC7, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC7, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 6;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON9:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC8, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC8, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 7;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON10:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC9, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC9, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 8;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON11:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC10, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC10, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 9;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON12:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC11, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC11, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 10;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;
			case IDC_BUTTON13:
				{
					int val = (int)GetDlgItemInt(hWnd, IDC_STATIC12, NULL, FALSE);
					SetDlgItemInt(hDlg, IDC_STATIC12, val ? 0 : 1, FALSE);

					CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
					cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
					cabinOptionMessage->option = 11;
					cabinOptionMessage->value = val ? 0 : 1;
					fSend(controller, cabinOptionMessage);
				}
				break;

			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}