#include "stdafx.h"
#include "CabinController__.h"

using namespace Cabin;

CabinController::CabinController(int cabinCount)
{
	currentCabinParameters = 0;
	CreateCabinController(cabinCount);
	CreateMessageHandler();
}

CabinController::~CabinController()
{
}

void CabinController::CreateMessageHandler()
{
	messageHandler = new MessageHandler();

	InitMessageHandlerStates();
	InitCommands();
	InitMessageHandlerStatesCommands();
}

void CabinController::CreateCabinController(int cabinCount)
{
	cabinStateMachine = new CabinStateMachine();

	InitCabinStateMachine(cabinCount);
}

void CabinController::Response(void *data)
{
	messageHandler->Response(data);
}

void CabinController::Request(void *data)
{
	messageHandler->Request(data);
}

void CabinController::SetRequestHandler(RequestHandler requestHandler)
{
	messageHandler->SetRequestHandler(requestHandler);
}

void CabinController::InitCommands()
{
	ParameterizedCommand* updateMessageHandlerStateCommand = new ParameterizedCommand();
	updateMessageHandlerStateCommand->SetAction(std::bind(&MessageHandler::UpdateStateAction, messageHandler, placeholders::_1));
	commands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, dynamic_cast<IParameterizedCommand*>(updateMessageHandlerStateCommand)));
	commands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, dynamic_cast<IParameterizedCommand*>(updateMessageHandlerStateCommand)));
	commands.insert(pair<int, IParameterizedCommand*>(MessageCode::PAUSE, dynamic_cast<IParameterizedCommand*>(updateMessageHandlerStateCommand)));

	ParameterizedCommand* setCabinOptionCommand = new ParameterizedCommand();
	setCabinOptionCommand->SetAction(std::bind(&CabinController::SetCabinOption, this, placeholders::_1));
	commands.insert(pair<int, IParameterizedCommand*>(MessageCode::SET_CABIN_OPTION, dynamic_cast<IParameterizedCommand*>(setCabinOptionCommand)));
}

void CabinController::InitMessageHandlerStates()
{
	list<IState*> messageHandlerLinkedStates;

	MessageHandlerState* startedState = new MessageHandlerStartedState();
	MessageHandlerState* stoppedState = new MessageHandlerStoppedState();
	MessageHandlerState* pausedState = new MessageHandlerPausedState();

	messageHandlerStates.clear();
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::STARTED, startedState));
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::STOPPED, stoppedState));
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::PAUSED, pausedState));

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STOPPED]);
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::PAUSED]);
	messageHandlerStates[MessageHandlerStateType::STARTED]->SetLinkedStates(messageHandlerLinkedStates);

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STOPPED]);
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STARTED]);
	messageHandlerStates[MessageHandlerStateType::PAUSED]->SetLinkedStates(messageHandlerLinkedStates);

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STARTED]);
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetLinkedStates(messageHandlerLinkedStates);
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetStateChangedEvent(std::bind(&CabinController::ReinitCabinStateMachineEvent, this, placeholders::_1));
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetStateChangedEventParameters(0);

	messageHandler->ChangeState(stoppedState);
}

void CabinController::InitMessageHandlerStatesCommands()
{
	map<int, IParameterizedCommand*> messageHandlerStateCommands;

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, commands[MessageCode::STOP]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::PAUSE, commands[MessageCode::PAUSE]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::SET_CABIN_OPTION, commands[MessageCode::SET_CABIN_OPTION]));
	messageHandlerStates[MessageHandlerStateType::STARTED]->SetCommands(messageHandlerStateCommands);

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, commands[MessageCode::STOP]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, commands[MessageCode::START]));
	messageHandlerStates[MessageHandlerStateType::PAUSED]->SetCommands(messageHandlerStateCommands);

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, commands[MessageCode::START]));
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetCommands(messageHandlerStateCommands);
}

void CabinController::InitCabinStateMachine(int cabinCount)
{
	this->cabinCount = cabinCount;
	switch(cabinCount)
	{
	case CabinCount::singleCabin:
		InitSingleCabin();
		break;
	case CabinCount::doubleCabin:
		InitDoubleCabin();
		break;
	}
	InitCurrentCabinParameters();
}

void CabinController::InitCurrentCabinParameters()
{
	if ( !currentCabinParameters )
	{
		currentCabinParameters = new CabinStateParameters();
	}

	switch(cabinCount)
	{
	case CabinCount::singleCabin:
		currentCabinParameters->SetCabinOptions(32, 7);
		break;
	case CabinCount::doubleCabin:
		currentCabinParameters->SetCabinOptions(1056, 12);
		break;
	}

	cabinStateMachine->ChangeState(*cabinStates.begin());
}

void CabinController::InitSingleCabin()
{
	InitSingleCabinStates();
	SetSingleCabinLinkedStates();
}

void CabinController::InitSingleCabinStates()
{
	enum CabinStateEventParamsType
	{
		EMPTY_PARAMTERS,
		DOOR1_OPEN,
		DOOR1_CLOSE,
		DOOR2_OPEN,
		DOOR2_CLOSE,
		DETECTOR1_ENABLE
	};

	enum EventType
	{
		EMPTY_EVENT,
		UPDATE_CABIN_OPTION_EVENT
	};

	map<int, CabinStateEventParams*> cabinStateEventParams;
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(EMPTY_PARAMTERS, 0));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR1_OPEN, new CabinStateEventParams(0, 0)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR1_CLOSE, new CabinStateEventParams(0, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR2_OPEN, new CabinStateEventParams(5, 0)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR2_CLOSE, new CabinStateEventParams(5, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DETECTOR1_ENABLE, new CabinStateEventParams(3, 1)));

	map<int, PatternEvent> cabinStateEvents;
	cabinStateEvents.insert(pair<int, PatternEvent>(EMPTY_EVENT, std::bind(&CabinController::EmptyEvent, this, placeholders::_1)));
	cabinStateEvents.insert(pair<int, PatternEvent>(UPDATE_CABIN_OPTION_EVENT, std::bind(&CabinController::UpdateCabinOption, this, placeholders::_1)));

	if (cabinStates.size())
	{
		cabinStates.clear();
	}

	AddCabinState(32, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 1100000
	AddCabinState(34, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 1100010
	AddCabinState(36, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 1100100
	AddCabinState(32, 7, cabinStateEvents[UPDATE_CABIN_OPTION_EVENT], cabinStateEventParams[DOOR1_CLOSE]);	// 1100000, EVENT:D1_CLOSE
	AddCabinState(33, 7, cabinStateEvents[UPDATE_CABIN_OPTION_EVENT], cabinStateEventParams[DETECTOR1_ENABLE]);	// 1100011, EVENT:W1_ENABLE
	AddCabinState(41, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 1101011
	AddCabinState(33, 7, cabinStateEvents[UPDATE_CABIN_OPTION_EVENT], cabinStateEventParams[DOOR2_OPEN]);	// 1100011, EVENT:D2_OPEN
	AddCabinState(1, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);		// 0000011
	AddCabinState(17, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 0010011
	AddCabinState(65, 7, cabinStateEvents[EMPTY_EVENT], cabinStateEventParams[EMPTY_PARAMTERS]);	// 1000011
	AddCabinState(1, 7, cabinStateEvents[UPDATE_CABIN_OPTION_EVENT], cabinStateEventParams[DOOR2_CLOSE]);	// 0000011, EVENT:D2_CLOSE
	AddCabinState(33, 7, cabinStateEvents[UPDATE_CABIN_OPTION_EVENT], cabinStateEventParams[DOOR1_OPEN]);	// 1100011, EVENT:D1_OPEN
}

void CabinController::SetSingleCabinLinkedStates()
{
	vector<vector<unsigned int>> linkedStatesVectorArray;
	vector<unsigned int> linkedStatesVector;

	linkedStatesVector.push_back(2);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(2);
	linkedStatesVector.push_back(0);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(3);
	linkedStatesVector.push_back(1);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(4);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(5);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(6);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(7);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(9);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(9);
	linkedStatesVector.push_back(7);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(10);
	linkedStatesVector.push_back(8);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(11);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	linkedStatesVector.push_back(0);
	linkedStatesVectorArray.push_back(linkedStatesVector);
	linkedStatesVector.clear();

	SetLinkedStates(linkedStatesVectorArray);
}

void CabinController::InitDoubleCabin()
{
	InitDoubleCabinStates();
	SetDoubleCabinLinkedStates();
}

void CabinController::InitDoubleCabinStates()
{
	enum CabinStateEventParamsType
	{
		EMPTY_PARAMTERS,
		DOOR1_OPEN,
		DOOR1_CLOSE,
		DOOR2_OPEN,
		DOOR2_CLOSE,
		DOOR3_OPEN,
		DOOR3_CLOSE,
		DETECTOR1_ENABLE,
		DETECTOR2_ENABLE
	};

	enum EventType
	{
		EMPTY_EVENT,
		UPDATE_CABIN_OPTION_EVENT
	};

	map<int, CabinStateEventParams*> cabinStateEventParams;
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(EMPTY_PARAMTERS, 0));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR1_OPEN, new CabinStateEventParams(0, 0)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR1_CLOSE, new CabinStateEventParams(0, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR2_OPEN, new CabinStateEventParams(5, 0)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR2_CLOSE, new CabinStateEventParams(5, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR3_OPEN, new CabinStateEventParams(10, 0)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DOOR3_CLOSE, new CabinStateEventParams(10, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DETECTOR1_ENABLE, new CabinStateEventParams(3, 1)));
	cabinStateEventParams.insert(pair<int, CabinStateEventParams*>(DETECTOR2_ENABLE, new CabinStateEventParams(8, 1)));

	map<int, PatternEvent> cabinStateEvents;
	cabinStateEvents.insert(pair<int, PatternEvent>(EMPTY_EVENT, std::bind(&CabinController::EmptyEvent, this, placeholders::_1)));
	cabinStateEvents.insert(pair<int, PatternEvent>(UPDATE_CABIN_OPTION_EVENT, std::bind(&CabinController::UpdateCabinOption, this, placeholders::_1)));

	if (cabinStates.size())
	{
		cabinStates.clear();
	}
}

void CabinController::SetDoubleCabinLinkedStates()
{
}

void CabinController::UpdateCabinOption(IPatternEventParameters* eventParameters)
{
	CabinStateEventParams* cabinStateEventParams = static_cast<CabinStateEventParams*>(eventParameters);
	CabinOptionMsg* cabinOptionMessage = new CabinOptionMsg();
	cabinOptionMessage->code = MessageCode::SET_CABIN_OPTION;
	cabinOptionMessage->option = cabinStateEventParams->optionIndex;
	cabinOptionMessage->value = cabinStateEventParams->optionValue;
	messageHandler->Request(static_cast<void*>(cabinOptionMessage));
}

void CabinController::EmptyEvent(IPatternEventParameters* eventParameters)
{
}

void CabinController::AddCabinState(unsigned int packedCabinOptions, size_t cabinOptionsCount,
	PatternEvent cabinStateChangedEvent, IPatternEventParameters* cabinStateChangedEventParameters)
{
	CabinState* cabinState = new CabinState();
	CabinStateParameters* cabinStateParameters = new CabinStateParameters();
	cabinStateParameters->SetCabinOptions(packedCabinOptions, cabinOptionsCount);
	cabinState->SetStateParameters(cabinStateParameters);
	cabinState->SetStateChangedEvent(cabinStateChangedEvent);
	cabinState->SetStateChangedEventParameters(cabinStateChangedEventParameters);
	cabinStates.push_back(cabinState);
}

void CabinController::SetCabinOption(IActionParameters* actionParameters)
{
	CabinOptionMsg* cabinOptionMessage = static_cast<CabinOptionMsg*>(actionParameters);
	currentCabinParameters->SetCabinOption(cabinOptionMessage->option, cabinOptionMessage->value);
	cabinStateMachine->Execute(currentCabinParameters);
}

void CabinController::SetLinkedStates(vector<vector<unsigned int>> linkedStatesVectorArray)
{
	list<IState*> linkedStates;
	for (unsigned int cabinStatesCounter = 0; cabinStatesCounter < linkedStatesVectorArray.size(); cabinStatesCounter++)
	{
		linkedStates.clear();
		vector<unsigned int> linkedStatesVector = linkedStatesVectorArray[cabinStatesCounter];
		for (unsigned int linkedStatesCounter = 0; linkedStatesCounter < linkedStatesVector.size(); linkedStatesCounter++)
		{
			linkedStates.push_back(cabinStates[linkedStatesVector[linkedStatesCounter]]);
		}
		cabinStates[cabinStatesCounter]->SetLinkedStates(linkedStates);
	}
}

void CabinController::ReinitCabinStateMachineEvent(IPatternEventParameters* eventParameters)
{
	InitCurrentCabinParameters();
}