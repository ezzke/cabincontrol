#include "stdafx.h"
#include "OptionLocker.h"

using namespace Misc;

void OptionLocker::SetValue(size_t optionIndex, bool value)
{
	options[optionIndex] = value;
}

void OptionLocker::SetValues(bool value)
{
	for (size_t i = 0; i < options.size(); i++)
	{
		SetValue(i, value);
	}
}

void OptionLocker::Lock(size_t optionIndex)
{
	SetValue(optionIndex, true);
}

void OptionLocker::Unlock(size_t optionIndex)
{
	SetValue(optionIndex, false);
}

bool OptionLocker::IsLocked(size_t optionIndex)
{
	return options[optionIndex];
}

void OptionLocker::UnlockAll()
{
	SetValues(false);
}

void OptionLocker::LockAll()
{
	SetValues(true);
}

OptionLocker::OptionLocker(size_t optionsCount, bool locked)
{
	for (size_t i = 0; i < optionsCount; i++)
	{
		options.push_back(locked);
	}
}

OptionLocker::~OptionLocker(void)
{
	options.clear();
}
