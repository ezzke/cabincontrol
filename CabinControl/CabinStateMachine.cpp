#include "stdafx.h"
#include "CabinStateMachine.h"

using namespace Cabin;

vector<int> BitUtils::unpack(unsigned int val, size_t count)
{
	vector<int> result;
	if ( count < sizeof(val) * 8 )
	{
		for ( size_t i = 0; i < count; i++ )
		{
			result.push_back((val & (1 << i)) ? 1 : 0);
		}
	}
	return result;
}


CabinDevicesStateActionParams::CabinDevicesStateActionParams(int optionIndex, int optionValue) 
{ 
	this->optionIndex = optionIndex;
	this->optionValue = optionValue;
}

CabinSensorStateActionParams::CabinSensorStateActionParams(int stateMachineId, int stateNumber)
{
	this->stateMachineId = stateMachineId;
	this->stateNumber = stateNumber;
}


CabinStateMachine::CabinStateMachine()
{
}

CabinStateMachine::~CabinStateMachine()
{
}

void CabinStateMachine::Execute(IStateParameters* stateParameters)
{
	CabinState* cabinState = dynamic_cast<CabinState*>(state);
	cabinState->Execute(this, stateParameters);
}

bool CabinStateParameters::Equals(IStateParameters* stateParameters)
{
	CabinStateParameters* cabinStateParameters = dynamic_cast<CabinStateParameters*>(stateParameters);

	bool isEquals = true;
	vector<int> compCabinOptions = cabinStateParameters->GetCabinOptions();
	for (unsigned int i = 0; ( i < compCabinOptions.size() ) && isEquals; i++)
	{
		if ( cabinOptions[i] != MININT32 )
		{
			if ( cabinOptions[i] != compCabinOptions[i] )
			{
				isEquals = false;
			}
		}
	}
	return isEquals;
}

void CabinStateParameters::SetCabinOptions(vector<int> cabinOptions)
{
	this->cabinOptions = cabinOptions;
}

vector<int> CabinStateParameters::GetCabinOptions()
{
	return cabinOptions;
}

void CabinStateParameters::SetCabinOption(int optionIndex, int optionValue)
{
	cabinOptions[optionIndex] = optionValue;
}

void CabinStateParameters::SetCabinOptions(unsigned int packedCabinOptions, size_t optionsCount)
{
	SetCabinOptions(BitUtils::unpack(packedCabinOptions, optionsCount));
}

void CabinState::Execute(IStateContext* stateContext, IStateParameters* stateParameters)
{
	IState* nextState = GetNextState(stateParameters);
	ChangeState(stateContext, nextState);
}


UpdateCabinDevicesStateActionParameters::UpdateCabinDevicesStateActionParameters(IStateContext* stateContext, IStateParameters* stateParameters)
{
	this->stateContext = stateContext;
	this->stateParameters = stateParameters;
}

UpdateOptionActionParameters::UpdateOptionActionParameters(int optionIndex, int optionValue)
{
	this->optionIndex = optionIndex;
	this->optionValue = optionValue;
}

DevicesCommandPair::DevicesCommandPair(IStateParameters* stateParameters, ParameterizedCommandPair* parameterizedCommandPair)
{
	this->stateParameters = stateParameters;
	this->parameterizedCommandPair = parameterizedCommandPair;
}

void CabinDevicesState::SetCommands(vector<DevicesCommandPair*> commands)
{
	this->commands = commands;
}

void CabinDevicesState::Execute(IStateContext* stateContext, IStateParameters* stateParameters)
{
	DevicesCommandPair* commandPair;
	ParameterizedCommandPair* parameterizedCommandPair;
	vector<DevicesCommandPair*>::iterator commandsIterator = commands.begin();
	bool completed = false;

	while ( (commandsIterator != commands.end()) && !completed )
	{
		commandPair = *commandsIterator;
		if (commandPair->stateParameters->Equals(stateParameters))
		{
			parameterizedCommandPair = commandPair->parameterizedCommandPair;
			parameterizedCommandPair->GetCommand()->Execute(parameterizedCommandPair->GetActionParameters());
			// ��������� ����
			completed = true;
		}
		commandsIterator++;
	}
	if ( !completed )
	{
		IState* nextState = GetNextState(stateParameters);
		ChangeState(stateContext, nextState);
	}
}

CabinBase::CabinBase()
{
}

CabinBase::~CabinBase()
{
	vector<CabinSensorsStateMachine*>::iterator cabinSensorsStateMachinesIterator = cabinSensorsStateMachines.begin();
	while( cabinSensorsStateMachinesIterator != cabinSensorsStateMachines.end() )
	{
		CabinSensorsStateMachine* cabinSensorsStateMachine = *cabinSensorsStateMachinesIterator;
		if (cabinSensorsStateMachine)
		{
			delete cabinSensorsStateMachine;
		}
		cabinSensorsStateMachinesIterator++;
	}
	cabinSensorsStateMachines.clear();

	if (devicesStateMachine)
	{
		delete devicesStateMachine;
	}
}

void CabinBase::UpdateCabinSensorsStateNumbers(IActionParameters* cabinSensorsActionParameters)
{
	CabinSensorStateActionParams* eventParameters = static_cast<CabinSensorStateActionParams*>(cabinSensorsActionParameters);
	cabinSensorsStateNumbers[eventParameters->stateMachineId] = eventParameters->stateNumber;
}

void CabinBase::ResetCabinSensorsStateMachine(int sensorsStateMachineNumber, IState* state)
{
	cabinSensorsStateMachines[sensorsStateMachineNumber]->ChangeState(state);
	cabinSensorsStateNumbers[sensorsStateMachineNumber] = 0;
}

void CabinBase::ResetDevicesStateMachine(IState* state)
{
	devicesStateMachine->ChangeState(state);
}

void CabinBase::SetCabinSensorsStateMachines(vector<CabinSensorsStateMachine*> cabinSensorsStateMachines)
{
	this->cabinSensorsStateMachines = cabinSensorsStateMachines;
}

void CabinBase::SetDeviceStateMachine(CabinDevicesStateMachine* devicesStateMachine)
{
	this->devicesStateMachine = devicesStateMachine;
}

void CabinBase::SetCabinSensorsStateNumbers(vector<int> cabinSensorsStateNumbers)
{
	this->cabinSensorsStateNumbers = cabinSensorsStateNumbers;
}

void CabinBase::SetCabinDevicesStateParameters(CabinDevicesStateParameters* cabinDevicesStateParameters)
{
	this->cabinDevicesStateParameters = cabinDevicesStateParameters;
}

void SingleCabin::Execute(IStateParameters* stateParameters)
{
	CabinStateParameters* cabinStateParameters = dynamic_cast<CabinStateParameters*>(stateParameters);
	vector<int> currentCabinOptions = cabinStateParameters->GetCabinOptions();

	CabinSensorsStateParameters* cabinSensorsStateParameters = new CabinSensorsStateParameters();

	cabinSensorsStateParameters->SetCabinOptions(0, 4);
	cabinSensorsStateParameters->SetCabinOption(0, currentCabinOptions[CabinOptions::SENSOR1]);
	cabinSensorsStateParameters->SetCabinOption(1, currentCabinOptions[CabinOptions::SENSOR2]);
	cabinSensorsStateParameters->SetCabinOption(2, currentCabinOptions[CabinOptions::SENSOR3]);
	cabinSensorsStateParameters->SetCabinOption(3, currentCabinOptions[CabinOptions::SENSOR4]);
	cabinSensorsStateMachines[CabinNumber::First]->Execute(cabinSensorsStateParameters);

	cabinDevicesStateParameters->SetCabinOption(0, cabinSensorsStateNumbers[CabinNumber::First]);
	cabinDevicesStateParameters->SetCabinOption(1, currentCabinOptions[CabinOptions::DOOR1]);
	cabinDevicesStateParameters->SetCabinOption(2, currentCabinOptions[CabinOptions::DETECTOR1]);
	cabinDevicesStateParameters->SetCabinOption(3, currentCabinOptions[CabinOptions::DOOR2]);
	devicesStateMachine->Execute(cabinDevicesStateParameters);
}

SingleCabin::SingleCabin()
{
	cabinSensorsStateNumbers.push_back(0);

	cabinDevicesStateParameters = new CabinDevicesStateParameters();
}

SingleCabin::~SingleCabin()
{
}
	
void DoubleCabin::Execute(IStateParameters* stateParameters)
{
	CabinStateParameters* cabinStateParameters = dynamic_cast<CabinStateParameters*>(stateParameters);
	vector<int> currentCabinOptions = cabinStateParameters->GetCabinOptions();

	CabinSensorsStateParameters* cabinSensorsStateParameters = new CabinSensorsStateParameters();

	cabinSensorsStateParameters->SetCabinOptions(0, 4);
	cabinSensorsStateParameters->SetCabinOption(0, currentCabinOptions[CabinOptions::SENSOR1]);
	cabinSensorsStateParameters->SetCabinOption(1, currentCabinOptions[CabinOptions::SENSOR2]);
	cabinSensorsStateParameters->SetCabinOption(2, currentCabinOptions[CabinOptions::SENSOR3]);
	cabinSensorsStateParameters->SetCabinOption(3, currentCabinOptions[CabinOptions::SENSOR4]);
	cabinSensorsStateMachines[CabinNumber::First]->Execute(cabinSensorsStateParameters);

	cabinSensorsStateParameters->SetCabinOptions(0, 4);
	cabinSensorsStateParameters->SetCabinOption(0, currentCabinOptions[CabinOptions::SENSOR4]);
	cabinSensorsStateParameters->SetCabinOption(1, currentCabinOptions[CabinOptions::SENSOR5]);
	cabinSensorsStateParameters->SetCabinOption(2, currentCabinOptions[CabinOptions::SENSOR6]);
	cabinSensorsStateParameters->SetCabinOption(3, currentCabinOptions[CabinOptions::SENSOR7]);
	cabinSensorsStateMachines[CabinNumber::Second]->Execute(cabinSensorsStateParameters);

	cabinDevicesStateParameters->SetCabinOption(0, cabinSensorsStateNumbers[CabinNumber::First]);
	cabinDevicesStateParameters->SetCabinOption(1, cabinSensorsStateNumbers[CabinNumber::Second]);
	cabinDevicesStateParameters->SetCabinOption(2, currentCabinOptions[CabinOptions::DOOR1]);
	cabinDevicesStateParameters->SetCabinOption(3, currentCabinOptions[CabinOptions::DETECTOR1]);
	cabinDevicesStateParameters->SetCabinOption(4, currentCabinOptions[CabinOptions::DOOR2]);
	cabinDevicesStateParameters->SetCabinOption(5, currentCabinOptions[CabinOptions::DETECTOR2]);
	cabinDevicesStateParameters->SetCabinOption(6, currentCabinOptions[CabinOptions::DOOR3]);
	devicesStateMachine->Execute(cabinDevicesStateParameters);
}

DoubleCabin::DoubleCabin()
{
	cabinSensorsStateNumbers.push_back(0);
	cabinSensorsStateNumbers.push_back(0);

	cabinDevicesStateParameters = new CabinDevicesStateParameters();
	cabinDevicesStateParameters->SetCabinOptions(80, 7);
}

DoubleCabin::~DoubleCabin()
{
}