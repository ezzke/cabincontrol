#include "stdafx.h"
#include "ActionParameters.h"

using namespace Cabin;

UnlockCommandActionParameters::UnlockCommandActionParameters(int optionIndex)
{
	this->optionIndex = optionIndex;
}

ResetSensorsStateMachineActionParameters::ResetSensorsStateMachineActionParameters(int sensorsStateMachineIndex)
{
	this->sensorsStateMachineIndex = sensorsStateMachineIndex;
}