//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by StateMachineTest.rc
//
#define IDC_MYICON                      2
#define IDD_STATEMACHINETEST_DIALOG     102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_STATEMACHINETEST            107
#define IDI_SMALL                       108
#define IDC_STATEMACHINETEST            109
#define IDR_MAINFRAME                   128
#define IDD_TEST_WINDOW                 129
#define IDC_START_BTN                   1000
#define IDC_STOP_BTN                    1001
#define IDC_PAUSE                       1002
#define IDC_PAUSE_BTN                   1002
#define IDC_BUTTON2                     1013
#define IDC_STATIC1                     1015
#define IDC_STATIC2                     1016
#define IDC_STATIC3                     1017
#define IDC_STATIC4                     1018
#define IDC_STATIC5                     1019
#define IDC_STATIC6                     1020
#define IDC_STATIC7                     1021
#define IDC_STATIC99                    1022
#define IDC_STATIC8                     1023
#define IDC_STATIC9                     1024
#define IDC_STATIC10                    1025
#define IDC_BUTTON3                     1026
#define IDC_STATIC11                    1027
#define IDC_STATIC12                    1028
#define IDC_BUTTON4                     1029
#define IDC_BUTTON5                     1030
#define IDC_BUTTON6                     1031
#define IDC_BUTTON7                     1032
#define IDC_BUTTON8                     1033
#define IDC_BUTTON9                     1034
#define IDC_BUTTON10                    1035
#define IDC_BUTTON11                    1036
#define IDC_BUTTON12                    1037
#define IDC_BUTTON13                    1038
#define IDC_STATIC0001                  1039
#define IDC_STATIC                      -1
#define IDC_STATIC666                   -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
