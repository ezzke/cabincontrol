#pragma once

#include "Command.h"
using namespace Pattern;

namespace Cabin
{

	class UnlockCommandActionParameters : public IActionParameters
	{
	public:
		int optionIndex;

		UnlockCommandActionParameters(int optionIndex);
	};

	class ResetSensorsStateMachineActionParameters : public IActionParameters
	{
	public:
		int sensorsStateMachineIndex;

		ResetSensorsStateMachineActionParameters(int sensorsStateMachineIndex);
	};

}
