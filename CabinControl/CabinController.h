#pragma once

#include "State.h"
#include "Command.h"

#include "CabinStateMachine.h"
#include "MessageHandler.h"
#include "OptionLocker.h"

#include "ActionParameters.h"

using namespace Pattern;
using namespace Misc;

namespace Cabin
{

	class CabinController
	{
	private:
		OptionLocker* commandsLocker;

		map<int, IParameterizedCommand*> messageHandlerCommands;
		map<int, MessageHandlerState*> messageHandlerStates;

		MessageHandler* messageHandler;

		void CreateMessageHandler();
		void CreateMessageHandlerCommands();
		void CreateMessageHandlerStates();
		void CreateMessageHandlerStatesCommands();

		int cabinType;
		CabinStateParameters* currentCabinParameters;

		vector<vector<CabinSensorsState*>> cabinSensorsStates;
		vector<CabinDevicesState*> cabinDevicesStates;
		CabinBase* cabinStateMachine;

		void ResetDevicesStateMachine();
		void ResetSensorsStateMachine(int cabinIndex);
		void ResetCabinOptions();
		void CreateSingleCabin();
		void CreateDoubleCabin();

		// ��������������� ������� ��� ������������� ���������
		vector<ParameterizedCommandPair*> CreateCabinSensorsStateEvents(ParameterizedAction action, 
			int cabinNumber, int stateId);

		CabinSensorsState* CreateCabinSensorsState(unsigned int packedCabinOptions, size_t optionsCount, 
			vector<ParameterizedCommandPair*> events);

		CabinDevicesStateParameters* CabinController::CreateDoubleCabinDevicesStateParameters(unsigned int packedCabinOptions, size_t optionsCount, int s1, int s2);
		CabinDevicesState* CreateDoubleCabinDevicesState(unsigned int packedCabinOptions, size_t optionsCount,
			vector<ParameterizedCommandPair*> events, vector<DevicesCommandPair*> commands);
		void InitDoubleCabinDevicesStates();
		void InitDoubleCabinDevicesLinkedStates();

		CabinSensorsStateMachine* CreateSensorsStateMachine(int cabinNumber);
		CabinDevicesStateMachine* CreateSingleDevicesStateMachine();
		CabinDevicesStateMachine* CreateDoubleDevicesStateMachine();

	private:
		void UpdateCabinOptionAction(IActionParameters* actionParameters);
		void SetCabinOptionAction(IActionParameters* actionParameters);
		void ResetCabinOptionsAction(IActionParameters* actionParameters);
		void ResetCabinStateAction(IActionParameters* actionParameters);
		void ResetSensorsStateMachineAction(IActionParameters* actionParameters);
		void UnlockCommandAction(IActionParameters* actionParameters);

	public:
		CabinController(int cabinCount);
		~CabinController();

		void Response(void *data);
		void Request(void *data);
		void SetRequestHandler(RequestHandler requestHandler);
	};

}