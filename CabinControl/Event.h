#pragma once
#include <functional>

using namespace std;

namespace Pattern
{

	class IPatternEventParameters
	{
	};

	typedef function<void(IPatternEventParameters*)> PatternEvent;

	class EventPair
	{
	public:
		IPatternEventParameters* patternEventParameters;
		PatternEvent patternEvent;

		EventPair(PatternEvent patternEvent, IPatternEventParameters* patternEventParameters)
		{
			this->patternEventParameters = patternEventParameters;
			this->patternEvent = patternEvent;
		}
	};

}