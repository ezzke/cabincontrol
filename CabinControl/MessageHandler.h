#pragma once
#include "State.h"
#include "Command.h"
#include "MessageData.h"
#include <map>

using namespace Pattern;
using namespace std;

namespace Cabin
{

	enum MessageHandlerStateType
	{
		STARTED,
		STOPPED,
		PAUSED
	};

	typedef void (*RequestHandler)(void *data);

	class MessageHandlerStateParameters : public StateParameters
	{
	private:
		int stateType;
		void SetStateType(int stateType);

	public:
		int GetStateType();
		bool Equals(IStateParameters* stateParameters);

		MessageHandlerStateParameters(int stateType);
	};

	class MessageHandlerState : public State
	{
	private:
		map<int, IParameterizedCommand*> commands;

	public:
		void Response(void *data);
		void SetCommands(map<int, IParameterizedCommand*> commands);
		void Execute(IStateContext* stateContext, IStateParameters* stateParameters);
	};

	class MessageHandlerStartedState : public MessageHandlerState
	{
	public:
		MessageHandlerStartedState();
	};

	class MessageHandlerStoppedState : public MessageHandlerState
	{
	public:
		MessageHandlerStoppedState();
	};

	class MessageHandlerPausedState : public MessageHandlerState
	{
	public:
		MessageHandlerPausedState();
	};

	class MessageHandler : public StateContext
	{
	private:
		RequestHandler requestHandler;
		void Execute(IStateParameters* stateParameters);

	public:
		void Response(void *data);
		void Request(void *data);
		void SetRequestHandler(RequestHandler requestHandler);
		void UpdateStateAction(IActionParameters* actionParameters);

		MessageHandler();
	};

}