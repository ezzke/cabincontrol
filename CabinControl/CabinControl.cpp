#include "stdafx.h"
#include "CabinControl.h"

CABINCONTROL_API void* CABIN_CreateCabinController(int cabinCount)
{
	return static_cast<void*>(new CabinController(cabinCount));
}

CABINCONTROL_API int CABIN_Send(void *cabinController, void *data)
{
	if (cabinController)
	{
		CabinController* controller = static_cast<CabinController*>(cabinController);
		controller->Response(data);
		return FALSE;
	}
	return TRUE;
}

CABINCONTROL_API int CABIN_SetRequestHandler(void *cabinController, RequestHandler handler)
{
	if (cabinController)
	{
		CabinController* controller = static_cast<CabinController*>(cabinController);
		controller->SetRequestHandler(handler);
		return FALSE;
	}
	return TRUE;
}
