#pragma once

#include <vector>

using namespace std;

namespace Misc
{

	class OptionLocker
	{
	private:
		vector<bool> options;

		void SetValue(size_t optionIndex, bool value);
		void SetValues(bool value);

	public:
		void Lock(size_t optionIndex);
		void Unlock(size_t optionIndex);

		bool IsLocked(size_t optionIndex);

		void UnlockAll();
		void LockAll();

		OptionLocker(size_t optionsCount, bool locked = false);
		~OptionLocker(void);
	};

}

