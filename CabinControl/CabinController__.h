#pragma once
#include "State.h"
#include "Command.h"

#include "CabinStateMachine__.h"
#include "MessageHandler.h"

using namespace Pattern;

namespace Cabin
{

	class CabinController
	{
	private:
		map<int, IParameterizedCommand*> commands;
		map<int, MessageHandlerState*> messageHandlerStates;
		vector<CabinState*> cabinStates;

		CabinStateParameters* currentCabinParameters;
		int cabinCount;

		MessageHandler* messageHandler;
		
		CabinStateMachine* cabinStateMachine;

		void CreateMessageHandler();
		void CreateCabinController(int cabinCount);

		void InitCommands();
		void InitMessageHandlerStates();
		void InitMessageHandlerStatesCommands();

		void InitCabinStateMachine(int cabinCount);
		void InitCurrentCabinParameters();
		void InitSingleCabin();
		void InitSingleCabinStates();
		void SetSingleCabinLinkedStates();

		void InitDoubleCabin();
		void InitDoubleCabinStates();
		void SetDoubleCabinLinkedStates();

		void AddCabinState(unsigned int packedCabinOptions, size_t cabinOptionsCount,
			PatternEvent cabinStateChangedEvent, IPatternEventParameters* cabinStateChangedEventParameters);
		void SetLinkedStates(vector<vector<unsigned int>> linkedStatesVectorArray);

		void SetCabinOption(IActionParameters* actionParameters);

		void UpdateCabinOption(IPatternEventParameters* eventParameters);
		void EmptyEvent(IPatternEventParameters* eventParameters);

		void ReinitCabinStateMachineEvent(IPatternEventParameters* eventParameters);

	public:
		CabinController(int cabinCount);
		~CabinController();

		void Response(void *data);
		void Request(void *data);
		void SetRequestHandler(RequestHandler requestHandler);
	};

}

