#include "stdafx.h"
#include "CabinController.h"

using namespace Cabin;
using namespace placeholders;

CabinController::CabinController(int cabinType)
{
	messageHandler = 0;
	currentCabinParameters = 0;

	CreateMessageHandler();

	this->cabinType = cabinType;
	ResetCabinOptions();

	switch (cabinType)
	{
	case CabinType::SINGLE:
		CreateSingleCabin();
		break;
	case CabinType::DOUBLE:
		CreateDoubleCabin();
		break;
	};
}

CabinController::~CabinController()
{
	if ( messageHandler )
	{
		delete messageHandler;
	}
}

void CabinController::Response(void *data)
{
	messageHandler->Response(data);
}

void CabinController::Request(void *data)
{
	messageHandler->Request(data);
}

void CabinController::SetRequestHandler(RequestHandler requestHandler)
{
	messageHandler->SetRequestHandler(requestHandler);
}

void CabinController::ResetDevicesStateMachine()
{
	cabinStateMachine->ResetDevicesStateMachine(cabinDevicesStates[0]);
}

void CabinController::ResetSensorsStateMachine(int cabinIndex)
{
	cabinStateMachine->ResetCabinSensorsStateMachine(cabinIndex, cabinSensorsStates[cabinIndex][0]);
}

void CabinController::ResetCabinOptions()
{
	if ( !currentCabinParameters )
	{
		currentCabinParameters = new CabinStateParameters();
	}

	vector<int> cabinOptions;

	switch(cabinType)
	{
	case CabinType::SINGLE:
		{
			cabinOptions.resize(7, 0);
			cabinOptions[5] = 1;
		}
		break;
	case CabinType::DOUBLE:
		{
			cabinOptions.resize(12, 0);
			cabinOptions[5] = 1;
			cabinOptions[10] = 1;
		}
		break;
	}

	currentCabinParameters->SetCabinOptions(cabinOptions);
}


void CabinController::UpdateCabinOptionAction(IActionParameters* actionParameters)
{
	UpdateOptionActionParameters* parameters = static_cast<UpdateOptionActionParameters*>(actionParameters);

	if ( !commandsLocker->IsLocked(parameters->optionIndex) )
	{
		commandsLocker->Lock(parameters->optionIndex);

		CabinOptionMsg* message = new CabinOptionMsg();
		message->code = MessageCode::SET_CABIN_OPTION;
		message->option = parameters->optionIndex;
		message->value = parameters->optionValue;

		messageHandler->Request(message);
	}
}

void CabinController::SetCabinOptionAction(IActionParameters* actionParameters)
{
	CabinOptionMsg* message = static_cast<CabinOptionMsg*>(actionParameters);
	currentCabinParameters->SetCabinOption(message->option, message->value);
	cabinStateMachine->Execute(currentCabinParameters);
}

void CabinController::ResetCabinOptionsAction(IActionParameters* actionParameters)
{
	ResetCabinOptions();
}

void CabinController::ResetCabinStateAction(IActionParameters* actionParameters)
{
	commandsLocker->UnlockAll();
	ResetCabinOptions();
	
	ResetDevicesStateMachine();

	switch (cabinType)
	{
	case CabinType::SINGLE:
		ResetSensorsStateMachine(0);
		break;
	case CabinType::DOUBLE:
		ResetSensorsStateMachine(0);
		ResetSensorsStateMachine(1);
		break;
	}
}

void CabinController::ResetSensorsStateMachineAction(IActionParameters* actionParameters)
{
	ResetSensorsStateMachineActionParameters* parameters = static_cast<ResetSensorsStateMachineActionParameters*>(actionParameters);
	ResetSensorsStateMachine(parameters->sensorsStateMachineIndex);
}

void CabinController::UnlockCommandAction(IActionParameters* actionParameters)
{
	UnlockCommandActionParameters* parameters = static_cast<UnlockCommandActionParameters*>(actionParameters);
	commandsLocker->Unlock(parameters->optionIndex);
}

void CabinController::CreateMessageHandler()
{
	messageHandler = new MessageHandler();

	CreateMessageHandlerStates();
	CreateMessageHandlerCommands();
	CreateMessageHandlerStatesCommands();
}

void CabinController::CreateMessageHandlerCommands()
{
	auto changeStateAction = std::bind(&MessageHandler::UpdateStateAction, messageHandler, _1);
	IParameterizedCommand* updateMessageHandlerStateCommand = new ParameterizedCommand(changeStateAction);
	messageHandlerCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, updateMessageHandlerStateCommand));
	messageHandlerCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, updateMessageHandlerStateCommand));
	messageHandlerCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::PAUSE, updateMessageHandlerStateCommand));

	auto setCabinOptionAction = std::bind(&CabinController::SetCabinOptionAction, this, _1);
	ParameterizedCommand* setCabinOptionCommand = new ParameterizedCommand(setCabinOptionAction);
	messageHandlerCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::SET_CABIN_OPTION, dynamic_cast<IParameterizedCommand*>(setCabinOptionCommand)));
}

void CabinController::CreateMessageHandlerStates()
{
	list<IState*> messageHandlerLinkedStates;

	MessageHandlerState* startedState = new MessageHandlerStartedState();
	MessageHandlerState* stoppedState = new MessageHandlerStoppedState();
	MessageHandlerState* pausedState = new MessageHandlerPausedState();

	messageHandlerStates.clear();
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::STARTED, startedState));
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::STOPPED, stoppedState));
	messageHandlerStates.insert(pair<int, MessageHandlerState*>(MessageHandlerStateType::PAUSED, pausedState));

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STOPPED]);
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::PAUSED]);
	messageHandlerStates[MessageHandlerStateType::STARTED]->SetLinkedStates(messageHandlerLinkedStates);

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STOPPED]);
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STARTED]);
	messageHandlerStates[MessageHandlerStateType::PAUSED]->SetLinkedStates(messageHandlerLinkedStates);

	messageHandlerLinkedStates.clear();
	messageHandlerLinkedStates.push_back(messageHandlerStates[MessageHandlerStateType::STARTED]);
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetLinkedStates(messageHandlerLinkedStates);

	messageHandler->ChangeState(stoppedState);
}

void CabinController::CreateMessageHandlerStatesCommands()
{
	// Execute actions
	map<int, IParameterizedCommand*> messageHandlerStateCommands;

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, messageHandlerCommands[MessageCode::STOP]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::PAUSE, messageHandlerCommands[MessageCode::PAUSE]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::SET_CABIN_OPTION, messageHandlerCommands[MessageCode::SET_CABIN_OPTION]));
	messageHandlerStates[MessageHandlerStateType::STARTED]->SetCommands(messageHandlerStateCommands);

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::STOP, messageHandlerCommands[MessageCode::STOP]));
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, messageHandlerCommands[MessageCode::START]));
	messageHandlerStates[MessageHandlerStateType::PAUSED]->SetCommands(messageHandlerStateCommands);

	messageHandlerStateCommands.clear();
	messageHandlerStateCommands.insert(pair<int, IParameterizedCommand*>(MessageCode::START, messageHandlerCommands[MessageCode::START]));
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetCommands(messageHandlerStateCommands);

	// Changed event actions
	auto resetCabinEvent = std::bind(&CabinController::ResetCabinStateAction, this, _1);
	ParameterizedCommand* resetCabinCommand = new ParameterizedCommand(resetCabinEvent);
	ParameterizedCommandPair* commandPair = new ParameterizedCommandPair(0, resetCabinCommand);
	vector<ParameterizedCommandPair*> stoppedStateEvents;
	stoppedStateEvents.push_back(commandPair);
	messageHandlerStates[MessageHandlerStateType::STOPPED]->SetStateChangedEvents(stoppedStateEvents);
}

void CabinController::CreateSingleCabin()
{
	commandsLocker = new OptionLocker(7);

	cabinStateMachine = new SingleCabin();
	
	vector<CabinSensorsStateMachine*> cabinSensorsStateMachines;
	cabinSensorsStateMachines.push_back(CreateSensorsStateMachine(CabinNumber::First));
	cabinStateMachine->SetCabinSensorsStateMachines(cabinSensorsStateMachines);

	cabinStateMachine->SetDeviceStateMachine(CreateSingleDevicesStateMachine());
}

void CabinController::CreateDoubleCabin()
{
	commandsLocker = new OptionLocker(12);

	cabinStateMachine = new DoubleCabin();

	vector<CabinSensorsStateMachine*> cabinSensorsStateMachines;
	cabinSensorsStateMachines.push_back(CreateSensorsStateMachine(CabinNumber::First));
	cabinSensorsStateMachines.push_back(CreateSensorsStateMachine(CabinNumber::Second));
	cabinStateMachine->SetCabinSensorsStateMachines(cabinSensorsStateMachines);

	cabinStateMachine->SetDeviceStateMachine(CreateDoubleDevicesStateMachine());

}

CabinDevicesStateParameters* CabinController::CreateDoubleCabinDevicesStateParameters(unsigned int packedCabinOptions, size_t optionsCount, int s1, int s2)
{
	CabinDevicesStateParameters* cabinDevicesStateParameters = new CabinDevicesStateParameters();
	cabinDevicesStateParameters->SetCabinOptions(packedCabinOptions, optionsCount);
	cabinDevicesStateParameters->SetCabinOption(0, s1);
	cabinDevicesStateParameters->SetCabinOption(1, s2);
	return cabinDevicesStateParameters;
}

CabinDevicesState* CabinController::CreateDoubleCabinDevicesState(unsigned int packedCabinOptions, size_t optionsCount, 
	vector<ParameterizedCommandPair*> events, vector<DevicesCommandPair*> commands)
{
	CabinDevicesState* cabinDevicesState = new CabinDevicesState();
	cabinDevicesState->SetStateParameters(CreateDoubleCabinDevicesStateParameters(packedCabinOptions, optionsCount, MININT32, MININT32));

	cabinDevicesState->SetStateChangedEvents(events);
	cabinDevicesState->SetCommands(commands);

	return cabinDevicesState;
}

vector<ParameterizedCommandPair*> CabinController::CreateCabinSensorsStateEvents(ParameterizedAction action, int cabinNumber, int stateId)
{
	IActionParameters* actionParameters = new CabinSensorStateActionParams(cabinNumber, stateId);
	IParameterizedCommand* command = new ParameterizedCommand(action);
	ParameterizedCommandPair* commandPair = new ParameterizedCommandPair(actionParameters, command);

	vector<ParameterizedCommandPair*> events;
	events.push_back(commandPair);

	return events;
}

CabinSensorsState* CabinController::CreateCabinSensorsState(unsigned int packedCabinOptions, size_t optionsCount, 
	vector<ParameterizedCommandPair*> events)
{
	CabinSensorsState* cabinSensorsState = new CabinSensorsState();
	CabinSensorsStateParameters* cabinSensorsStateParameters = new CabinSensorsStateParameters();
	cabinSensorsStateParameters->SetCabinOptions(packedCabinOptions, optionsCount);
	cabinSensorsState->SetStateParameters(cabinSensorsStateParameters);

	cabinSensorsState->SetStateChangedEvents(events);

	return cabinSensorsState;
}

CabinSensorsStateMachine* CabinController::CreateSensorsStateMachine(int cabinNumber)
{
	vector<CabinSensorsState*> sensorsStates;
	list<IState*> linkedStates;

	auto updateCabinSensorsStateNumbers = bind(&DoubleCabin::UpdateCabinSensorsStateNumbers, cabinStateMachine, _1);
	
	// �������� ���������
	sensorsStates.push_back(CreateCabinSensorsState(0, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 0)));
	sensorsStates.push_back(CreateCabinSensorsState(1, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 1)));
	sensorsStates.push_back(CreateCabinSensorsState(2, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 2)));
	sensorsStates.push_back(CreateCabinSensorsState(0, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 3)));
	sensorsStates.push_back(CreateCabinSensorsState(4, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 4)));
	sensorsStates.push_back(CreateCabinSensorsState(8, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 5)));
	sensorsStates.push_back(CreateCabinSensorsState(0, 4, CreateCabinSensorsStateEvents(updateCabinSensorsStateNumbers, cabinNumber, 6)));
	cabinSensorsStates.push_back(sensorsStates);

	// ���������� ���������
	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][1]);
	cabinSensorsStates[cabinNumber][0]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][0]);
	linkedStates.push_back(cabinSensorsStates[cabinNumber][2]);
	cabinSensorsStates[cabinNumber][1]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][3]);
	cabinSensorsStates[cabinNumber][2]->SetLinkedStates(linkedStates);
	 
	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][4]);
	cabinSensorsStates[cabinNumber][3]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][3]);
	linkedStates.push_back(cabinSensorsStates[cabinNumber][5]);
	cabinSensorsStates[cabinNumber][4]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][4]);
	linkedStates.push_back(cabinSensorsStates[cabinNumber][6]);
	cabinSensorsStates[cabinNumber][5]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinSensorsStates[cabinNumber][5]);
	cabinSensorsStates[cabinNumber][6]->SetLinkedStates(linkedStates);

	// �������� �������� ��������� �������� � ��������� ���������� ���������
	CabinSensorsStateMachine* sensorsStateMachine = new CabinSensorsStateMachine();
	sensorsStateMachine->ChangeState(cabinSensorsStates[cabinNumber][0]);

	return sensorsStateMachine;
}

CabinDevicesStateMachine* CabinController::CreateSingleDevicesStateMachine()
{
	//vector<CabinDevicesState*> devicesStates;

	//vector<EventPair*> events;
	//vector<CommandPair*> commands;

	//devicesStates.push_back(CreateCabinDevicesState(80, 7, MININT, MININT, events, commands));

	return 0;
}

void CabinController::InitDoubleCabinDevicesStates()
{
	vector<ParameterizedCommandPair*> events;
	vector<DevicesCommandPair*> commands;

	enum DoubleCabinCommands
	{
		D1_open, D1_close, D2_open, D2_close, D3_open, D3_close, W1_enable, W2_enable,
		D1_unlock, D2_unlock, D3_unlock, W1_unlock, W2_unlock,
		S1_default, S2_default
	};

	ParameterizedCommand* updateCabinOptionCommand = new ParameterizedCommand(bind(&CabinController::UpdateCabinOptionAction, this, _1));
	ParameterizedCommand* unlockCommandCommand = new ParameterizedCommand(bind(&CabinController::UnlockCommandAction, this, _1));
	ParameterizedCommand* resetSensorsStateMachineCommand = new ParameterizedCommand(bind(&CabinController::ResetSensorsStateMachineAction, this, _1));

	map<int, IActionParameters*> doubleCabinActionParametersDictionary;
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D1_open, new UpdateOptionActionParameters(CabinOptions::DOOR1, 1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D1_close, new UpdateOptionActionParameters(CabinOptions::DOOR1, 0)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D2_open, new UpdateOptionActionParameters(CabinOptions::DOOR2, 1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D2_close, new UpdateOptionActionParameters(CabinOptions::DOOR2, 0)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D3_open, new UpdateOptionActionParameters(CabinOptions::DOOR3, 1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D3_close, new UpdateOptionActionParameters(CabinOptions::DOOR3, 0)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(W1_enable, new UpdateOptionActionParameters(CabinOptions::DETECTOR1, 1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(W2_enable, new UpdateOptionActionParameters(CabinOptions::DETECTOR2, 1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D1_unlock, new UnlockCommandActionParameters(CabinOptions::DOOR1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D1_unlock, new UnlockCommandActionParameters(CabinOptions::DOOR1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D2_unlock, new UnlockCommandActionParameters(CabinOptions::DOOR2)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(D3_unlock, new UnlockCommandActionParameters(CabinOptions::DOOR3)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(W1_unlock, new UnlockCommandActionParameters(CabinOptions::DETECTOR1)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(W2_unlock, new UnlockCommandActionParameters(CabinOptions::DETECTOR2)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(S1_default, new ResetSensorsStateMachineActionParameters(CabinNumber::First)));
	doubleCabinActionParametersDictionary.insert(pair<int, IActionParameters*>(S2_default, new ResetSensorsStateMachineActionParameters(CabinNumber::Second)));

	map<int, ParameterizedCommandPair*> doubleCabinEventsDictionary;
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D1_open, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D1_open], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D2_open, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D2_open], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D3_open, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D3_open], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D1_close, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D1_close], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D2_close, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D2_close], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D3_close, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D3_close], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(W1_enable, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[W1_enable], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(W2_enable, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[W2_enable], updateCabinOptionCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D1_unlock, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D1_unlock], unlockCommandCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D2_unlock, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D2_unlock], unlockCommandCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(D3_unlock, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[D3_unlock], unlockCommandCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(W1_unlock, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[W1_unlock], unlockCommandCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(W2_unlock, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[W2_unlock], unlockCommandCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(S1_default, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[S1_default], resetSensorsStateMachineCommand)));
	doubleCabinEventsDictionary.insert(pair<int, ParameterizedCommandPair*>(S2_default, new ParameterizedCommandPair(doubleCabinActionParametersDictionary[S2_default], resetSensorsStateMachineCommand)));

	// �������� ��������� ��� DD

	//#1
	events.clear();
	events.push_back(doubleCabinEventsDictionary[S2_default]);
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(80, 7, 3, MININT32), doubleCabinEventsDictionary[D1_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(80, 7, events, commands));

	//#2
	events.clear();
	events.push_back(doubleCabinEventsDictionary[S2_default]);
	events.push_back(doubleCabinEventsDictionary[D1_unlock]);
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	events.push_back(doubleCabinEventsDictionary[W1_enable]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#3
	events.clear();
	events.push_back(doubleCabinEventsDictionary[S2_default]);
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	events.push_back(doubleCabinEventsDictionary[W1_unlock]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(92, 7, events, commands));

	//#4
	events.clear();
	events.push_back(doubleCabinEventsDictionary[S2_default]);
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	events.push_back(doubleCabinEventsDictionary[D2_open]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#5
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D2_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(68, 7, MININT32, 3), doubleCabinEventsDictionary[D2_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(68, 7, events, commands));

	//#6
	events.clear();
	events.push_back(doubleCabinEventsDictionary[S1_default]);
	events.push_back(doubleCabinEventsDictionary[D2_unlock]);
	events.push_back(doubleCabinEventsDictionary[W2_enable]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#7
	events.clear();
	events.push_back(doubleCabinEventsDictionary[W2_unlock]);
	events.push_back(doubleCabinEventsDictionary[D1_open]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(116, 7, events, commands));

	//#8
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D1_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(112, 7, 3, MININT32), doubleCabinEventsDictionary[D1_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(112, 7, events, commands));

	//#9
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D1_unlock]);
	events.push_back(doubleCabinEventsDictionary[W1_enable]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(116, 7, events, commands));

	//#10
	events.clear();
	events.push_back(doubleCabinEventsDictionary[W1_unlock]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(124, 7, events, commands));

	//#11
	events.clear();
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(116, 7, events, commands));

	//#12
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D3_open]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#13
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D1_unlock]);
	events.push_back(doubleCabinEventsDictionary[D3_open]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(80, 7, 3, MININT32), doubleCabinEventsDictionary[D1_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(80, 7, events, commands));

	//#14
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D1_unlock]);
	events.push_back(doubleCabinEventsDictionary[W1_enable]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#15
	events.clear();
	events.push_back(doubleCabinEventsDictionary[W1_unlock]);
	events.push_back(doubleCabinEventsDictionary[D3_open]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(92, 7, events, commands));

	//#16
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D3_open]);
	commands.clear();
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(84, 7, events, commands));

	//#17
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(16, 7, 6, MININT32), doubleCabinEventsDictionary[D1_close]));
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(16, 7, MININT32, 6), doubleCabinEventsDictionary[D3_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(16, 7, events, commands));

	//#18
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	events.push_back(doubleCabinEventsDictionary[W1_enable]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(20, 7, MININT32, 6), doubleCabinEventsDictionary[D3_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(20, 7, events, commands));

	//#19
	events.clear();
	events.push_back(doubleCabinEventsDictionary[W1_unlock]);
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(28, 7, MININT32, 6), doubleCabinEventsDictionary[D3_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(28, 7, events, commands));

	//#20
	events.clear();
	events.push_back(doubleCabinEventsDictionary[D3_unlock]);
	commands.clear();
	commands.push_back(new DevicesCommandPair(CreateDoubleCabinDevicesStateParameters(20, 7, MININT32, 6), doubleCabinEventsDictionary[D3_close]));
	cabinDevicesStates.push_back(CreateDoubleCabinDevicesState(20, 7, events, commands));
}

void CabinController::InitDoubleCabinDevicesLinkedStates()
{
	list<IState*> linkedStates;

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[1]);
	cabinDevicesStates[0]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[2]);
	cabinDevicesStates[1]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[3]);
	cabinDevicesStates[2]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[4]);
	cabinDevicesStates[3]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[5]);
	cabinDevicesStates[4]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[6]);
	cabinDevicesStates[5]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[7]);
	linkedStates.push_back(cabinDevicesStates[11]);
	cabinDevicesStates[6]->SetLinkedStates(linkedStates);
	
	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[8]);
	linkedStates.push_back(cabinDevicesStates[12]);
	cabinDevicesStates[7]->SetLinkedStates(linkedStates);
	
	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[9]);
	linkedStates.push_back(cabinDevicesStates[13]);
	cabinDevicesStates[8]->SetLinkedStates(linkedStates);	
	
	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[10]);
	linkedStates.push_back(cabinDevicesStates[14]);
	cabinDevicesStates[9]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[15]);
	cabinDevicesStates[10]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[12]);
	cabinDevicesStates[11]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[13]);
	linkedStates.push_back(cabinDevicesStates[16]);
	cabinDevicesStates[12]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[14]);
	linkedStates.push_back(cabinDevicesStates[17]);
	cabinDevicesStates[13]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[15]);
	linkedStates.push_back(cabinDevicesStates[18]);
	cabinDevicesStates[14]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[19]);
	cabinDevicesStates[15]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[17]);
	linkedStates.push_back(cabinDevicesStates[0]);
	cabinDevicesStates[16]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[18]);
	linkedStates.push_back(cabinDevicesStates[1]);
	cabinDevicesStates[17]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[19]);
	linkedStates.push_back(cabinDevicesStates[2]);
	cabinDevicesStates[18]->SetLinkedStates(linkedStates);

	linkedStates.clear();
	linkedStates.push_back(cabinDevicesStates[3]);
	cabinDevicesStates[19]->SetLinkedStates(linkedStates);
}

CabinDevicesStateMachine* CabinController::CreateDoubleDevicesStateMachine()
{
	InitDoubleCabinDevicesStates();
	InitDoubleCabinDevicesLinkedStates();

	CabinDevicesStateMachine* stateMachine = new CabinDevicesStateMachine();
	stateMachine->ChangeState(cabinDevicesStates[0]);

	return stateMachine;
}