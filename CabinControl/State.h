#pragma once

#include <list>
#include <vector>
//#include "Event.h"
#include "Command.h"

using namespace std;

namespace Pattern
{

	class IStateContext;

	class IStateParameters
	{
	public:
		virtual bool Equals(IStateParameters* stateParameters) = 0;
	};

	class IState
	{
	public:
		virtual void ChangeState(IStateContext* stateContext, IState* state) = 0;
		virtual IState* GetNextState(IStateParameters* stateParameters) = 0;
		virtual void SetStateParameters(IStateParameters* stateParameters) = 0;
		virtual IStateParameters* GetStateParameters() = 0;
		virtual void SetLinkedStates(list<IState*> linkedStates) = 0;

		virtual void SetStateChangedEvents(vector<ParameterizedCommandPair*> stateChagnedEvents) = 0;
		virtual void StateChanged() = 0;
	};

	class IStateContext
	{
	public:
		virtual void ChangeState(IState* state) = 0;
	};

	class StateParameters : public IStateParameters
	{
	};

	class State : public IState
	{
	private:
		IStateParameters* stateParameters;
		list<IState*> linkedStates;

		vector<ParameterizedCommandPair*> stateChagnedEvents;

	public:
		void ChangeState(IStateContext* stateContext, IState* newState);
		IState* GetNextState(IStateParameters* stateParameters);
		void SetStateParameters(IStateParameters* stateParameters);
		IStateParameters* GetStateParameters();
		void SetLinkedStates(list<IState*> linkedStates);

		void SetStateChangedEvents(vector<ParameterizedCommandPair*> stateChagnedEvents);
		virtual void StateChanged();
	};

	class StateContext : public IStateContext
	{
	protected:
		IState* state;
	public:
		void ChangeState(IState* newState);
	};

}