// lib_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

#include <vector>

typedef void (*Handler)(void *data);

typedef void* (*fpCreateCabinController)(int);
typedef int (*fpSetRequestHandler)(void*, Handler);
typedef int (*fpSend)(void*, void*);

#define LIBNAME L"CabinControl.dll"
#define CREATE_CABIN_CONTROLLER "CABIN_CreateCabinController"
#define SEND "CABIN_Send"
#define SET_HANDLER "CABIN_SetRequestHandler"

enum MessageCode
{
	START,
	STOP,
	PAUSE,
	SET_CABIN_OPTION
};

class MessageDataBase
{
public:
	int code;
};

class MessageHandlerStateMsg : public MessageDataBase
{
};

void RequestHandler(void *data)
{
	printf("HOHOHO");
}

int main(int argc, wchar_t** argv)
{
	HINSTANCE hInstLib = 0;
	fpCreateCabinController fCreateCabinController = 0;
	fpSend fSend = 0;
	fpSetRequestHandler fSetRequest = 0;

	hInstLib = LoadLibrary( LIBNAME );
	if ( !hInstLib )
	{
		return ERROR_APP_INIT_FAILURE;
	}

	fCreateCabinController = ( fpCreateCabinController ) GetProcAddress( hInstLib, CREATE_CABIN_CONTROLLER );
	if ( !fCreateCabinController )
	{
		return ERROR_APP_INIT_FAILURE;
	}

	fSend= ( fpSend ) GetProcAddress( hInstLib, SEND );
	if ( !fSend )
	{
		return ERROR_APP_INIT_FAILURE;
	}

	fSetRequest = ( fpSetRequestHandler ) GetProcAddress( hInstLib, SET_HANDLER );
	if ( !fSetRequest )
	{
		return ERROR_APP_INIT_FAILURE;
	}

	void* controller = fCreateCabinController(1);
	fSetRequest(controller, RequestHandler);

	MessageHandlerStateMsg* mshm = new MessageHandlerStateMsg;
	mshm->code = MessageCode::START;
	fSend(controller, mshm);
	mshm->code = MessageCode::PAUSE;
	fSend(controller, mshm);
	mshm->code = MessageCode::STOP;
	fSend(controller, mshm);

	return EXIT_SUCCESS;
}

