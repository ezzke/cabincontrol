#include "stdafx.h"
#include "Command.h"

using namespace Pattern;

void IParameterizedCommand::operator()(IActionParameters* actionParameters)
{
	Execute(actionParameters);
}

void ISimpleCommand::operator()(void)
{
	Execute();
}


bool ParameterizedCommand::IsEmpty()
{
	return action._Empty();
}

void ParameterizedCommand::SetAction(ParameterizedAction action)
{
	this->action = action;
}

void ParameterizedCommand::Execute(IActionParameters* actionParameters)
{
	action(actionParameters);
}

ParameterizedCommand::ParameterizedCommand(ParameterizedAction action)
{
	SetAction(action);
}


bool SimpleCommand::IsEmpty()
{
	return action._Empty();
}

void SimpleCommand::SetAction(SimpleAction action)
{
	this->action = action;
}

void SimpleCommand::Execute()
{
	action();
}

SimpleCommand::SimpleCommand(SimpleAction action)
{
	SetAction(action);
}


IActionParameters* ParameterizedCommandPair::GetActionParameters()
{
	return actionParameters;
}

IParameterizedCommand* ParameterizedCommandPair::GetCommand()
{
	return command;
}

ParameterizedCommandPair::ParameterizedCommandPair(IActionParameters* actionParameters, IParameterizedCommand* command)
{
	this->actionParameters = actionParameters;
	this->command = command;
}