#pragma once
#include "State.h"
#include "Command.h"
#include "Event.h"
#include <vector>

using namespace Pattern;

namespace Cabin2
{

	using namespace std;

	enum CabinCount
	{
		singleCabin = 1,
		doubleCabin
	};

	// TODO:
	// ������� ���������
	class BitUtils
	{
	public:
		static vector<int> unpack(unsigned int val, size_t count);
	};

	class CabinStateEventParams : public IPatternEventParameters
	{
	public:
		int optionIndex;
		int optionValue;

		CabinStateEventParams(int optionIndex, int optionValue);
	};

	class CabinStateParameters : public StateParameters
	{
	private:
		vector<int> cabinOptions;

	public:
		bool Equals(IStateParameters* stateParameters);
		void SetCabinOptions(vector<int> cabinOptions);
		void SetCabinOptions(unsigned int packedCabinOptions, size_t optionsCount);
		vector<int> GetCabinOptions();
		void SetCabinOption(int optionIndex, int optionValue);
	};

	class CabinState : public State
	{
	public:
		void Execute(IStateContext* stateContext, IStateParameters* stateParameters);
	};

	class CabinStateMachine : public StateContext
	{
	public:
		CabinStateMachine();
		~CabinStateMachine();

		void Execute(IStateParameters* stateParameters);
	};

}