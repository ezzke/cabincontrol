#include "stdafx.h"
#include "CabinStateMachine__.h"

using namespace Cabin;

CabinStateMachine::CabinStateMachine()
{
}

CabinStateMachine::~CabinStateMachine()
{
}

void CabinStateMachine::Execute(IStateParameters* stateParameters)
{
	dynamic_cast<CabinState*>(state)->Execute(this, stateParameters);
}

bool CabinStateParameters::Equals(IStateParameters* stateParameters)
{
	return ( cabinOptions == dynamic_cast<CabinStateParameters*>(stateParameters)->GetCabinOptions() );
}

void CabinStateParameters::SetCabinOptions(vector<int> cabinOptions)
{
	this->cabinOptions = cabinOptions;
}

vector<int> CabinStateParameters::GetCabinOptions()
{
	return cabinOptions;
}

void CabinStateParameters::SetCabinOption(int optionIndex, int optionValue)
{
	cabinOptions[optionIndex] = optionValue;
}

void CabinStateParameters::SetCabinOptions(unsigned int packedCabinOptions, size_t optionsCount)
{
	SetCabinOptions(BitUtils::unpack(packedCabinOptions, optionsCount));
}

CabinStateEventParams::CabinStateEventParams(int optionIndex, int optionValue) 
{ 
	this->optionIndex = optionIndex;
	this->optionValue = optionValue;
}

void CabinState::Execute(IStateContext* stateContext, IStateParameters* stateParameters)
{
	IState* nextState = GetNextState(dynamic_cast<IStateParameters*>(stateParameters));
	ChangeState(stateContext, nextState);
}

vector<int> BitUtils::unpack(unsigned int val, size_t count)
{
	vector<int> result;
	if ( count < sizeof(val) * 8 )
	{
		for ( size_t i = 0; i < count; i++ )
		{
			result.push_back((val & (1 << i)) ? 1 : 0);
		}
	}
	return result;
}