#include "stdafx.h"
#include "MessageHandler.h"

using namespace Cabin;

MessageHandler::MessageHandler()
{
	requestHandler = 0;
}

void MessageHandlerState::Response(void *data)
{
	MessageDataBase* message = MessageParser::Parse(data);

	if ( commands.find(message->code) != commands.end() )
	{
		commands[message->code]->Execute(message);
	}
}

void MessageHandlerState::SetCommands(map<int, IParameterizedCommand*> commands)
{
	this->commands = commands;
}

void MessageHandlerState::Execute(IStateContext* stateContext, IStateParameters* stateParameters)
{
	IState* nextState = GetNextState(stateParameters);
	ChangeState(stateContext, nextState);
}

void MessageHandler::Response(void *data)
{
	MessageHandlerState* messageHandlerState = dynamic_cast<MessageHandlerState*>(state);
	messageHandlerState->Response(data);
}

void MessageHandler::Request(void *data)
{
	if ( requestHandler && data )
	{
		requestHandler(data);
	}
}

void MessageHandler::SetRequestHandler(RequestHandler requestHandler)
{
	this->requestHandler = requestHandler;
}

void MessageHandler::Execute(IStateParameters* stateParameters)
{
	MessageHandlerState* messageHandlerState = dynamic_cast<MessageHandlerState*>(state);
	messageHandlerState->Execute(this, stateParameters);
}

void MessageHandler::UpdateStateAction(IActionParameters* actionParameters)
{
	MessageHandlerStateMsg* message = static_cast<MessageHandlerStateMsg*>(actionParameters);
	MessageHandlerStateParameters* parameters = new MessageHandlerStateParameters(message->code);

	Execute(parameters);
}

MessageHandlerStateParameters::MessageHandlerStateParameters(int stateType)
{
	SetStateType(stateType);
}

int MessageHandlerStateParameters::GetStateType()
{
	return stateType;
}

void MessageHandlerStateParameters::SetStateType(int stateType)
{
	this->stateType = stateType;
}

bool MessageHandlerStateParameters::Equals(IStateParameters* stateParameters)
{
	MessageHandlerStateParameters* parameters = dynamic_cast<MessageHandlerStateParameters*>(stateParameters);
	return ( stateType == parameters->GetStateType() );
}

MessageHandlerStartedState::MessageHandlerStartedState()
{
	MessageHandlerStateParameters* parameters = 
		new MessageHandlerStateParameters(MessageHandlerStateType::STARTED);
	SetStateParameters(parameters);
}

MessageHandlerStoppedState::MessageHandlerStoppedState()
{
	MessageHandlerStateParameters* parameters =
		new MessageHandlerStateParameters(MessageHandlerStateType::STOPPED);
	SetStateParameters(parameters);
}

MessageHandlerPausedState::MessageHandlerPausedState()
{
	MessageHandlerStateParameters* parameters =
		new MessageHandlerStateParameters(MessageHandlerStateType::PAUSED);
	SetStateParameters(parameters);
}