#pragma once
#include <functional>

using namespace std;

namespace Pattern
{

	class IActionParameters
	{
	};

	typedef function<void(void)> SimpleAction;
	typedef function<void(IActionParameters*)> ParameterizedAction;

	class ISimpleCommand
	{
	public:
		virtual bool IsEmpty() = 0;
		virtual void Execute() = 0;
		virtual void SetAction(SimpleAction action) = 0;
		void operator()(void);
	};

	class IParameterizedCommand
	{
	public:
		virtual bool IsEmpty() = 0;
		virtual void Execute(IActionParameters* actionParameters) = 0;
		virtual void SetAction(ParameterizedAction action) = 0;
		void operator()(IActionParameters* actionParameters);
	};

	class ParameterizedCommand : public IParameterizedCommand
	{
	private:
		ParameterizedAction action;
	public:
		bool IsEmpty();
		void Execute(IActionParameters* actionParameters);
		void SetAction(ParameterizedAction action);

		ParameterizedCommand(ParameterizedAction action);
	};

	class SimpleCommand
	{
	private:
		SimpleAction action;
	public:
		bool IsEmpty();
		void Execute();
		void SetAction(SimpleAction action);

		SimpleCommand(SimpleAction action);
	};

	class ParameterizedCommandPair
	{
	private:
		IActionParameters* actionParameters;
		IParameterizedCommand* command;
	public:
		IActionParameters* GetActionParameters();
		IParameterizedCommand* GetCommand();

		ParameterizedCommandPair(IActionParameters* actionParameters, IParameterizedCommand* command);
	};
}